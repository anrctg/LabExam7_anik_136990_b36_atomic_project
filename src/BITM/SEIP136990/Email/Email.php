<?php

namespace App\Email;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class Email extends DB
{
    public $id;
    public $email;
    public $password;

    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION))
            session_start();
    }
    public function setData($postVaribaleData=NULL)
    {
        if(array_key_exists("id",$postVaribaleData))
        {
            $this->id = $postVaribaleData['id'];
        }
        if(array_key_exists("email",$postVaribaleData))
        {
            $this->email = $postVaribaleData['email'];
        }
        if(array_key_exists("password",$postVaribaleData))
        {
            $this->password = md5($postVaribaleData['password']);
        }

    }//end of set data
    public function store()
    {
        $arrData = array($this->email, $this->password);
        $sql = "INSERT into email(email,password) VALUES (?,?)";
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        if ($result) {
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        }
        else {
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");
        }
            Utility::redirect('create.php');

    }

}