
<?php


require_once("../../../vendor/autoload.php");

use App\Message\Message;

//echo Message::getMessage();

//echo Message::message();



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create</title>
    <link rel="stylesheet" href="../resource/css/bootstrap.min.css">
    <script src="../resource/js/bootstrap.min.js"></script>
</head>
<style>
    body{
        padding-top: 20px;
        background-color: #0f0f0f;
        background: url("../resource/img/bg1.jpg") no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
</style>
<body>


<div class="container">
    <div class="row centered-form text-center" style="margin-top: 12%">
        <p style="color: #31b0d5;text-align: center" id="message">
            <?php
            echo Message::message();
            ?>
        </p>
        <h2 style="color: #a6e1ec">Create.php</h2>
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Book Title</h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="store.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="tt"></label>
                            <input type="text" name="name" id="name" class="form-control input-sm" placeholder="Enter your name">
                        </div>
                        <div class="form-group">
                            <input type="file" name="profile_photo" id="profile_photo">
                        </div>
                        <input type="submit" value="Create" class="btn btn-info btn-block">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
